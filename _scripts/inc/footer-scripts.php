<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="/bs3/js/bootstrap.min.js"></script>
<script src="/js/iCheck/jquery.icheck.min.js"></script>
<script src="/_scripts/js/tinymce/tinymce.min.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script>
	$(document).ready(function(){
		$("#resource_form").validate();
	    tinymce.init({
	    	mode: "textareas",
	        selector: '.text-editor',
	        plugins : 'link',
	        menubar : false,
	        setup: function(editor) {
				editor.on('change', function(e) {
					tinyMCE.triggerSave();
					$("#" + editor.id).valid();
			    });
			}
	    });
	    $('input').iCheck({
	        checkboxClass: 'icheckbox_square-blue',
	        radioClass: 'iradio_square-blue',
	        increaseArea: '20%'
	    });
	});
</script>