<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Paradigm Life">
<link rel="shortcut icon" href="/images/favicon.png">
<title><?php echo $pageName;?></title>
<!--Core CSS -->
<link href="/bs3/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/bootstrap-reset.css" rel="stylesheet">
<link href="/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="/js/iCheck/skins/square/blue.css" rel="stylesheet" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="/css/jquery.ui.plupload.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="/css/style.css" rel="stylesheet">
<link href="/css/style-responsive.css" rel="stylesheet" />
<link href="/css/blue-theme.css" rel="stylesheet" />
<link href="/css/custom.css" rel="stylesheet" />
<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]>    
<script src="js/ie8-responsive-file-warning.js"></script>
<![endif]-->
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body>
<section id="container" >
  <header class="header fixed-top clearfix">
    <div class="brand">
      <a href="index.html" class="logo"><img src="/images/logo.png" alt=""></a>
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
      </div>
    </div>

    <div class="top-nav clearfix">
      <ul class="nav pull-right top-menu">
        <li class="dropdown">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#"><img alt="" src="/images/avatar1_small.jpg"><span class="username">Jim Ostler</span><b class="caret"></b></a>
          <ul class="dropdown-menu extended logout">
            <li><a href="#"><i class="fa fa-cog"></i>Settings</a></li>
            <li><a href="login.html"><i class="fa fa-key"></i>Log Out</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </header>
  <aside>
    <div id="sidebar" class="nav-collapse">
      <div class="leftside-navigation">
        <ul class="sidebar-menu" id="nav-accordion">
          <li><a href="home.php" class="<?php echo ($pageName == "Home" ? "active" : "");?>"><i class="fa fa-home"></i> <span>Home</span></a></li>
          <li><a href="/resources/" class="<?php echo ($pageName == "Resources" ? "active" : "");?>"><i class="fa fa-archive"></i> <span>Resources</span></a></li>
          <li><a href="webinars.php" class="<?php echo ($pageName == "Webinars" ? "active" : "");?>"><i class="fa fa-play-circle"></i> <span>Webinars</span></a></li>
          <li><a href="convertVideo.php" class="<?php echo ($pageName == "Video Encoder" ? "active" : "");?>"><i class="fa fa-film"></i> <span>Video Encoder</span></a></li>
        </ul>
      </div>
    </div>
  </aside>
