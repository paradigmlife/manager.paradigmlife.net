<?php
	
	function isEnabled($field)
	{
		$enabled = ($field == 1) ? '<div class="state icheckbox_square-blue checked disabled"></div>' : '<div class="state icheckbox_square-blue disabled"></div>';

		return $enabled;
	}

	function checkChecked($variable)
	{
		if(!isset($variable))
		{
			return 0;
		}

		return 1;
	}

	function isChecked($field)
	{
		$checked = ($field == 1) ? 'checked' : '';

		return $checked;
	}

	function isSelected($field, $match)
	{
		return $field == $match ? 'selected="selected"' : '';
	}
