<?php

require('../functions/core.php');
require('../class/Resource.class.php');
$resource = new Resource();

$resource->resourceId = $_POST['resource_id'];
$resource->deleteResource();

header('Location: /resources/'); 
exit;