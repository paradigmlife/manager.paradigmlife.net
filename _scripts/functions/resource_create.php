<?php

require('../functions/core.php');
require('../class/Resource.class.php');
$resource = new Resource();

$is_enabled = checkChecked($_POST['is_enabled']);
$is_featured = checkChecked($_POST['is_featured']);

$resourceData = array(
	'is_enabled'		=>	$is_enabled,
	'title'				=>	$_POST['title'],
	'subtitle'			=>	$_POST['subtitle'],
	'url_ref'			=>	$_POST['url_ref'],
	'why'				=>	$_POST['why'],
	'how'				=>	$_POST['how'],
	'what'				=>	$_POST['what'],
	'file_type'			=>	$_POST['file_type'],
	'short_desc'		=>	$_POST['short_desc'],
	'long_desc'			=>	$_POST['long_desc'],
	'image'				=>	$resource->s3Url.$_POST['image'],
	'large_image'		=>	$resource->s3Url.$_POST['large_image'],
	'resource_link'		=>	$resource->s3Url.$_POST['resource_link'],
	'is_featured'		=>	$is_featured,
	'lead_source'		=>	$_POST['lead_source'],
);

$resource->resourceData = $resourceData;
$resource->createResource();

header('Location: /resources/'); 
exit;