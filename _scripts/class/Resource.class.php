<?php

require("DBconnect.class.php");

$db = new DBconnect();
$mysql = $db->connectToDb(); 

class Resource {

	public $resourceData;
	public $s3Url = 'https://d2hqpb7xtno8dz.cloudfront.net/images/';
	public $resourceId;

	public function getAllResources()
	{
		global $mysql;

		$query = $mysql->prepare('SELECT * FROM resources');
		$query->execute();

		$result = $query->get_result();

		$allResources = array();
		
		while($row = $result->fetch_assoc()){
			$allResources[] = $row;
		}

		$query->close();
		$mysql->close();

		return $allResources;
	}

	public function getResource($id)
	{
		global $mysql;

		$query = $mysql->prepare('SELECT * FROM resources WHERE id = ?');
		$query->bind_param("i", $id);
		$query->execute();

		$result = $query->get_result();

		$resource = array();
		
		while($row = $result->fetch_assoc()){
			$resource = $row;
		}

		$query->close();
		$mysql->close();

		return $resource;
	}

	public function createResource()
	{
		global $mysql;

		$query = $mysql->prepare('INSERT INTO resources (is_enabled, title, subtitle, url_ref, why, how, what, short_desc, long_desc, image, large_image, resource_link, is_featured, lead_source) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
		$query->bind_param("isssssssssssis", $is_enabled, $title, $subtitle, $url_ref, $why, $how, $what, $short_desc, $long_desc, $image, $large_image, $resource_link, $is_featured, $lead_source);

		$is_enabled		= $this->resourceData['is_enabled'];
		$title			= $this->resourceData['title'];
		$subtitle		= $this->resourceData['subtitle'];
		$url_ref		= $this->resourceData['url_ref'];
		$why			= $this->resourceData['why'];
		$how			= $this->resourceData['how'];
		$what			= $this->resourceData['what'];
		$short_desc		= $this->resourceData['short_desc'];
		$long_desc		= $this->resourceData['long_desc'];
		$image			= $this->resourceData['image'];
		$large_image	= $this->resourceData['large_image'];
		$resource_link	= $this->resourceData['resource_link'];
		$is_featured	= $this->resourceData['is_featured'];
		$lead_source	= $this->resourceData['lead_source'];

		$query->execute();
		$query->close();
		$mysql->close();
	}

	public function updateResource()
	{
		global $mysql;

		$query = $mysql->prepare('UPDATE resources SET is_enabled=?, title=?, subtitle=?, url_ref=?, why=?, how=?, what=?, short_desc=?, long_desc=?, image=?, large_image=?, resource_link=?, is_featured=?, lead_source=? WHERE id = ?');
		$query->bind_param("isssssssssssisi", $is_enabled, $title, $subtitle, $url_ref, $why, $how, $what, $short_desc, $long_desc, $image, $large_image, $resource_link, $is_featured, $lead_source, $resource_id);

		$is_enabled		= $this->resourceData['is_enabled'];
		$title			= $this->resourceData['title'];
		$subtitle		= $this->resourceData['subtitle'];
		$url_ref		= $this->resourceData['url_ref'];
		$why			= $this->resourceData['why'];
		$how			= $this->resourceData['how'];
		$what			= $this->resourceData['what'];
		$short_desc		= $this->resourceData['short_desc'];
		$long_desc		= $this->resourceData['long_desc'];
		$image			= $this->resourceData['image'];
		$large_image	= $this->resourceData['large_image'];
		$resource_link	= $this->resourceData['resource_link'];
		$is_featured	= $this->resourceData['is_featured'];
		$lead_source	= $this->resourceData['lead_source'];
		$resource_id	= $this->resourceId;

		$query->execute();
		$query->close();
		$mysql->close();
	}

	public function deleteResource()
	{
		global $mysql;

		$query = $mysql->prepare('DELETE FROM resources WHERE id = ?');
		$query->bind_param("i", $id);
		
		$id = $this->resourceId;

		$query->execute();
		$query->close();
		$mysql->close();
	}
}