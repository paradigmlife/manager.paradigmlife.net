<?php
    $pageName = "Resources"; 
    require("../_scripts/inc/header.php");
    require("../_scripts/functions/core.php");
    require("../_scripts/class/Resource.class.php");

    $resources = new Resource();
    $resourcesData = $resources->getAllResources();
?>
    <section id="main-content">
        <section class="wrapper">
            <section class="panel">
                <header class="panel-heading">
                    All Resources
                </header>
                <div class="panel-body">
                    <a href="/resources/create.php" class="btn btn-primary">Add Resource</a>
                    <table id="resources-list" class="table table-hover general-table table-responsive">
                        <thead>
                            <tr>
                                <th>Icon</th>
                                <th>Name</th>
                                <th>Lead Source</th>
                                <th>Enabled</th>
                                <th>Featured</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($resourcesData as $resource): ?>
                            <tr class="resource" data-resource-id="/resources/edit.php?id=<?php echo $resource['id']; ?>">
                                <td><a href="/resources/edit.php?id=<?php echo $resource['id']; ?>"><img src="<?php echo $resource['image']; ?>" class="icon" /></a></td>
                                <td><a href="/resources/edit.php?id=<?php echo $resource['id']; ?>"><?php echo $resource['title']; ?></a></td>
                                <td><a href="/resources/edit.php?id=<?php echo $resource['id']; ?>"><?php echo $resource['lead_source']; ?></a></td>
                                <td><?php echo isEnabled($resource['is_enabled']); ?></td>
                                <td><?php echo isEnabled($resource['is_featured']); ?></a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </section>
        </section>
    </section>
</section>
    <?php require("../_scripts/inc/footer-scripts.php"); ?>
    <script>
        $(document).ready(function(){
            $("tr.resource").on("click", function() {
                console.log('Row Clicked!');
                window.document.location = $(this).data("resource-id");
            });
        });
    </script>
</body>
</html>

