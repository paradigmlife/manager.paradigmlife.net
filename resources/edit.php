<?php
    $pageName = "Edit Resource"; 
    require("../_scripts/inc/header.php");
    require("../_scripts/functions/core.php");
    require("../_scripts/class/Resource.class.php");

    // important variables that will be used throughout this example
    $bucket = 'paradigm-resources';
    $cloudfront = 'd2hqpb7xtno8dz';

    // these can be found on your Account page, under Security Credentials > Access Keys
    $accessKeyId = 'AKIAIDPEGUK2OE45BY3Q';
    $secret = 'LbUEpilf5/jdBnROo/Y90KHnF9ElartgcrghQDvg';

    // prepare policy
    $policy = base64_encode(json_encode(array(
        // ISO 8601 - date('c'); generates uncompatible date, so better do it manually
        'expiration' => date('Y-m-d\TH:i:s.000\Z', strtotime('+1 day')),  
        'conditions' => array(
            array('bucket' => $bucket),
            array('acl' => 'public-read'),
            array('starts-with', '$key', ''),
            // for demo purposes we are accepting only images
            array('starts-with', '$Content-Type', 'image/'),
            // Plupload internally adds name field, so we need to mention it here
            array('starts-with', '$name', ''),  
            // One more field to take into account: Filename - gets silently sent by FileReference.upload() in Flash
            // http://docs.amazonwebservices.com/AmazonS3/latest/dev/HTTPPOSTFlash.html
            array('starts-with', '$Filename', ''), 
        )
    )));

    // sign policy
    $signature = base64_encode(hash_hmac('sha1', $policy, $secret, true));

    $resourceId = htmlspecialchars($_GET['id']);

    $resource = new Resource();
    $resourceData = $resource->getResource($resourceId);
?>
    <section id="main-content">
        <section class="wrapper">
            <section class="panel">
            <?php if(empty($resourceData)): ?>
                <header class="panel-heading">
                    Error
                </header>
                <div class="panel-body">
                    <h2>Resource Not Found</h2>
                </div>
            <?php else: ?>
                <header class="panel-heading">
                    <?php echo $resourceData['title']; ?>
                </header>
                <div class="panel-body">
                    <div>
                        <form id="resource_form" class="cmxform" role="form" method="POST" action="/_scripts/functions/resource_update.php">
                            <input type="hidden" name="resource_id" class="url" id="resource_id" value="<?php echo htmlspecialchars($resourceData['id']); ?>">
                            <div id="enabled_boxes" class="form-group">
                                <label for="is_enabled">
                                    <input name="is_enabled" type="checkbox" id="is_enabled" value="is_enabled" <?php echo isChecked($resourceData['is_enabled']); ?>> Enabled
                                </label>
                                <label for="is_featured">
                                    <input name="is_featured" type="checkbox" id="is_featured" value="is_featured" <?php echo isChecked($resourceData['is_featured']); ?>> Featured
                                </label>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" type="title" class="form-control" id="title" value="<?php echo htmlspecialchars($resourceData['title']); ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="subtitle">Subtitle</label>
                                <input type="text" name="subtitle" class="form-control" id="subtitle" value="<?php echo htmlspecialchars($resourceData['subtitle']); ?>" required>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lead_source">Lead Source</label>
                                        <input type="text" name="lead_source" class="form-control" id="lead_source" value="<?php echo htmlspecialchars($resourceData['lead_source']); ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="url_ref">URL Ref</label>
                                        <input type="text" name="url_ref" class="form-control" id="url_ref" value="<?php echo htmlspecialchars($resourceData['url_ref']); ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="title">Resource Type</label>
                                        <select name="file_type" id="file_type" class="form-control">
                                            <option <?php echo isSelected($resourceData['file_type'], 'article'); ?>>Article</option>
                                            <option <?php echo isSelected($resourceData['file_type'], 'audio'); ?>>Audio</option>
                                            <option <?php echo isSelected($resourceData['file_type'], 'document'); ?>>Document</option>
                                            <option <?php echo isSelected($resourceData['file_type'], 'video'); ?>>Video</option>
                                        </select>
                                    </div>
                                </div>                                
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="why">Why</label>
                                        <textarea rows="5" name="why" class="form-control" id="why" required><?php echo htmlspecialchars($resourceData['why']); ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="how">How</label>
                                        <textarea rows="5" name="how" class="form-control" id="how" required><?php echo htmlspecialchars($resourceData['how']); ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="what">What</label>
                                        <textarea rows="5" name="what" class="form-control" id="what" required><?php echo htmlspecialchars($resourceData['what']); ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="short_desc">Short Description</label>
                                <textarea name="short_desc" class="text-editor" id="short_desc"><?php echo htmlspecialchars($resourceData['short_desc']); ?></textarea>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="long_desc">Long Description</label>
                                <textarea name="long_desc" class="text-editor" id="long_desc"><?php echo htmlspecialchars($resourceData['long_desc']); ?></textarea>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="large_image">Large Image</label>
                                <div class="current_url">
                                    <a href="<?php echo htmlspecialchars($resourceData['large_image']); ?>" target="_blank"><img class="large_image" src="<?php echo htmlspecialchars($resourceData['large_image']); ?>"/></div>
                                </div>
                                <input type="hidden" name="large_image" class="url" id="large_image">
                                <div class="uploaders" id="upload_lg-img">
                                    <p>Your browser doesn't have HTML5 support.</p>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="image">Main Image</label>
                                <div class="current_url">
                                    <a href="<?php echo htmlspecialchars($resourceData['image']); ?>" target="_blank"><img class="image" src="<?php echo htmlspecialchars($resourceData['image']); ?>"/></a>
                                </div>
                                <input type="hidden" name="image" class="url" id="image">
                                <div class="uploaders" id="upload_sm-img">
                                    <p>Your browser doesn't have HTML5 support.</p>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="resource">Resource</label>
                                <div class="current_url"><?php echo htmlspecialchars($resourceData['resource_link']); ?></div>
                                <input type="hidden" name="resource_link" class="url" id="resource_link">
                                <div class="uploaders" id="upload_resource">
                                    <p>Your browser doesn't have HTML5 support.</p>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info">Update Resource</button>
                        </form>
                        <form role="form" id="delete_resource" method="POST" action="/_scripts/functions/resource_delete.php" onsubmit="return confirm('Are you sure you want to delete this resource?');">
                            <input type="hidden" name="resource_id" class="url" id="resource_id" value="<?php echo htmlspecialchars($resourceData['id']); ?>">
                            <button type="submit" class="btn btn-danger">Delete Resource</button>
                        </form>
                    </div>
                </div>                
            <?php endif; ?>
            </section>
        </section>
    </section>
</section>
    <?php require("../_scripts/inc/footer-scripts.php"); ?>
    <script src="/js/plupload.full.min.js"></script>
    <script src="/js/jquery.ui.plupload.min.js"></script>

    <script type="text/javascript">
        $(".uploaders").each(function(index, element){
            $(function() {
                $('#' + element.id).plupload({
                    runtimes : 'html5',
                    url : 'https://<?php echo $bucket; ?>.s3.amazonaws.com:443/',
                    buttons:{
                        browse: true,
                        start: false,
                        stop: false
                    },
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'Content-Type': 'image/jpeg',
                        'AWSAccessKeyId' : '<?php echo $accessKeyId; ?>',       
                        'policy': '<?php echo $policy; ?>',
                        'signature': '<?php echo $signature; ?>'
                    },
                    file_data_name: 'file',
                    filters : {
                        max_file_size : '10mb',
                        mime_types: [
                            {title : "Image files", extensions : "jpg,jpeg"}
                        ]
                    },

                    init : {
                       FileUploaded: function(up, file, info) {
                            // Called when a file has finished uploading
                            $("#" + element.id).parent().find('.url').val(file.name);
                        },
                        
                        FilesAdded: function(up, files){
                             up.start();
                        },

                        Error: function(uploader, error){
                            console.log(error.message);
                        }
                    }
                });
            });
        });
    </script>    
</body>
</html>

