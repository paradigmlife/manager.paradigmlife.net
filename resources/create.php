<?php
    $pageName = "Create Resource"; 
    require("../_scripts/inc/header.php");
    require("../_scripts/functions/core.php");
    require("../_scripts/class/Resource.class.php");

    $bucket = 'paradigm-resources';
    $cloudfront = 'd2hqpb7xtno8dz';

    $accessKeyId = 'AKIAIDPEGUK2OE45BY3Q';
    $secret = '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';
    $policy = base64_encode(json_encode(array(
        'expiration' => date('Y-m-d\TH:i:s.000\Z', strtotime('+1 day')),  
        'conditions' => array(
            array('bucket' => $bucket),
            array('acl' => 'public-read'),
            array('starts-with', '$key', ''),
            array('starts-with', '$Content-Type', 'image/'),
            array('starts-with', '$name', ''),  
            array('starts-with', '$Filename', ''), 
        )
    )));
    $signature = base64_encode(hash_hmac('sha1', $policy, $secret, true));
?>
    <section id="main-content">
        <section class="wrapper">
            <section class="panel">
                <header class="panel-heading">
                    Create Resource
                </header>
                <div class="panel-body">
                    <div>
                        <form id="resource_form" class="cmxform" role="form" method="POST" action="/_scripts/functions/resource_create.php">
                            <div id="enabled_boxes" class="form-group">
                                <label for="is_enabled">
                                    <input name="is_enabled" type="checkbox" id="is_enabled" value="is_enabled"> Enabled
                                </label>
                                <label for="is_featured">
                                    <input name="is_featured" type="checkbox" id="is_featured" value="is_featured"> Featured
                                </label>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" name="title" type="title" class="form-control" id="title" required>
                            </div>
                            <div class="form-group">
                                <label for="subtitle">Subtitle</label>
                                <input type="text" name="subtitle" class="form-control" id="subtitle" required>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lead_source">Lead Source</label>
                                        <input type="text" name="lead_source" class="form-control" id="lead_source" maxlength="20" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="url_ref">URL Ref</label>
                                        <input type="text" name="url_ref" class="form-control" id="url_ref" maxlength="20" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="title">Resource Type</label>
                                        <select name="file_type" id="file_type" class="form-control">
                                            <option>Article</option>
                                            <option>Audio</option>
                                            <option>Document</option>
                                            <option>Video</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="why">Why</label>
                                        <textarea rows="5" name="why" class="form-control" id="why" required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="how">How</label>
                                        <textarea rows="5" name="how" class="form-control" id="how" required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="what">What</label>
                                        <textarea rows="5" name="what" class="form-control" id="what" required></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="short_desc">Short Description</label>
                                <textarea name="short_desc" class="text-editor" id="short_desc" required></textarea>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="long_desc">Long Description</label>
                                <textarea name="long_desc" class="text-editor" id="long_desc" required></textarea>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="large_image">Large Image</label>
                                <input type="hidden" name="large_image" class="url" id="large_image">
                                <div class="uploaders" id="upload_lg-img">
                                    <p>Your browser doesn't have HTML5 support.</p>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="image">Main Image</label>
                                <input type="hidden" name="image" class="url" id="image">
                                <div class="uploaders" id="upload_sm-img">
                                    <p>Your browser doesn't have HTML5 support.</p>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="resource">Resource</label>
                                <input type="hidden" name="resource_link" class="url" id="resource_link">
                                <div class="uploaders" id="upload_resource">
                                    <p>Your browser doesn't have HTML5 support.</p>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-info">Create</button>
                        </form>
                    </div>
                </div>
            </section>
        </section>
    </section>
</section>
    <?php require("../_scripts/inc/footer-scripts.php"); ?>
    <script src="/js/plupload.full.min.js"></script>
    <script src="/js/jquery.ui.plupload.min.js"></script>

    <script type="text/javascript">
        $(".uploaders").each(function(index, element){
            $(function() {
                $('#' + element.id).plupload({
                    runtimes : 'html5',
                    url : 'https://<?php echo $bucket; ?>.s3.amazonaws.com:443/',
                    buttons:{
                        browse: true,
                        start: false,
                        stop: false
                    },
                    multipart: true,
                    multipart_params: {
                        'key': '${filename}',
                        'Filename': '${filename}',
                        'acl': 'public-read',
                        'Content-Type': 'image/jpeg',
                        'AWSAccessKeyId' : '<?php echo $accessKeyId; ?>',       
                        'policy': '<?php echo $policy; ?>',
                        'signature': '<?php echo $signature; ?>'
                    },
                    file_data_name: 'file',
                    filters : {
                        max_file_size : '10mb',
                        mime_types: [
                            {title : "Image files", extensions : "jpg,jpeg"}
                        ]
                    },

                    init : {
                       FileUploaded: function(up, file, info) {
                            // Called when a file has finished uploading
                            $("#" + element.id).parent().find('.url').val(file.name);
                        },
                        
                        FilesAdded: function(up, files){
                             up.start();
                        },

                        Error: function(uploader, error){
                            console.log(error.message);
                        }
                    }
                });
            });
        });

    $(function() {
        var validator = $("#resource_form").submit(function() {
            // update underlying textarea before submit validation
            tinyMCE.triggerSave();
        }).validate({
            ignore: "",
            rules: {
                short_desc: "required",
                long_desc: "required"
            },
            errorPlacement: function(label, element) {
                // position error label after generated textarea
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            }
        });
        validator.focusInvalid = function() {
            // put focus on tinymce on submit validation
            if (this.settings.focusInvalid) {
                try {
                    var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                    if (toFocus.is("textarea")) {
                        tinyMCE.get(toFocus.attr("id")).focus();
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch (e) {
                    // ignore IE throwing errors when focusing hidden elements
                }
            }
        }
    });
    </script>
    </script>
</body>
</html>

