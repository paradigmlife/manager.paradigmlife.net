<?php
	$pageName = "Home"; 
	include "_scripts/inc/header.php"; 
?>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-md-4">
                    <section class="panel menubox">
                        <a href="/resources/" class="<?php echo ($pageName == "Resources" ? "active" : "");?>">
                            <div class="panel-body">
                                <section class="panel text-center">
                                    <div class="user-heading alt wdgt-row">
                                        <i class="fa fa-archive"></i>
                                    </div>
                                    <div class="panel-body">
                                        <div class="wdgt-value">
                                            <h1 class="count">Resources</h1>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </a>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="panel menubox">
                        <a href="webinars.php" class="<?php echo ($pageName == "Webinars" ? "active" : "");?>">
                            <div class="panel-body">
                                <section class="panel text-center">
                                    <div class="user-heading alt wdgt-row">
                                        <i class="fa fa-play-circle"></i>
                                    </div>
                                    <div class="panel-body">
                                        <div class="wdgt-value">
                                            <h1 class="count">Webinars</h1>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </a>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="panel menubox">
                        <a href="convertVideo.php" class="<?php echo ($pageName == "Video Encoder" ? "active" : "");?>">
                            <div class="panel-body">
                                <section class="panel text-center">
                                    <div class="user-heading alt wdgt-row">
                                        <i class="fa fa-film"></i>
                                    </div>
                                    <div class="panel-body">
                                        <div class="wdgt-value">
                                            <h1 class="count">Video Encoder</h1>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </a>
                    </section>
                </div>
            </div>
        </section>
    </section>
</section>
    <script src="js/jquery.js"></script>
    <script src="bs3/js/bootstrap.min.js"></script>
</body>
</html>

